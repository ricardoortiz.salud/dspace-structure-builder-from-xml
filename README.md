# Importar estructura a DSpace con strcture buiulder

Crear archivo XML con la siguiente estructura:

```
<import_structure>
    <community>
        <name>Primer Nivel</name>
        <description>Descripción</description>
        <community>
            <name>Segundo Nivel</name>
            <description>Descripción</description>
            <collection>
                <name>Biblioteca</name>
                <description>Descripción</description>
            </collection>
        </community>
    </community>
</import_structure>
```

Ejecutar el siguiente comando:

```
/dspace/bin/dspace structure-builder -f /dspace/bin/scripts/source.xml -o /dspace/bin/scripts/output.xml -e correo@admin.com
```

Donde source.xml es el archivo con la estructura que queremos importar y output.xml es el archivo de respuesta con la estrucura importada con los id correspondientes.